#include <iostream>
using namespace std;

int power(int value, int n){
    if(n<0){
        return 0;
    }
    if(n==0){
        return 1;
    }
    else{
        return value*power(value, n-1);
    }
}


int main(){

    cout<<power(2,2)<<endl;

    return 0;
}

