#include <QApplication>
#include <time.h>

#include "tp2.h" 

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	uint j;
	Array& sorted=w->newArray(toSort.size());
	// insertion sort from toSort to sorted
	toSort=sorted; // update the original array
	sorted[0] = toSort[0];
	for(uint i=1; i<toSort.size();i++){
		sorted[i] = toSort[i];
		for(j = 1; j<toSort.size();j++ )	{
			if(toSort[j] > sorted[j]){
				toSort[j+1]=toSort[j];
			}
		}
		toSort[j+1]=sorted[i];
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
