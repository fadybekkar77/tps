#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;
 

void bubbleSort(Array& toSort){
	// bubbleSort
    uint iNext;
    for(uint i= 0; i< toSort.size(); i++){
        iNext = i;
        for(uint j=0; j<toSort.size(); j++){
			if(toSort[j]>toSort[i]){
                iNext = j;
                toSort.swap(i,iNext);
			}
		}
	}
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
